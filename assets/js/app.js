$(document).foundation();

$(function() {
  $("#interstitial").click(function() {
    $(this).addClass("begone");
    
    setTimeout(function() {
      $("#interstitial").hide();
    }, 1000);
  });

  $(".room-label").click(function() {
    $(this).parent().children(".room-label").css("background-color", "transparent");
    $(this).css("background-color", $(this).css("border-color"));
    $(".canvas .room[data-id="+$(this).data("id")+"]").parent().children(".room").fadeOut();
    $(".canvas .room[data-id="+$(this).data("id")+"]").fadeIn();

    if ($(".canvas-info img[data-id="+$(this).data("id")+"]").length > 0) {
      $(".canvas-info img[data-id="+$(this).data("id")+"]").parent().children("img").fadeOut();
      $(".canvas-info img[data-id="+$(this).data("id")+"]").fadeIn();
    }

    if ($(".canvas-info a[data-id="+$(this).data("id")+"]").length > 0) {
      $(".canvas-info a[data-id="+$(this).data("id")+"]").parent().children("a").fadeOut();
      $(".canvas-info a[data-id="+$(this).data("id")+"]").fadeIn();
    }
  });

  $.each($(".canvas"), function(index, elem) {
    var $m = $(elem).width();
    $(elem).height($m);
    $(elem).children("img").width($m).height($m);
    $(elem).find(".room:eq(0)").show();
  });

  $.each($(".buttons"), function(index, elem) {
    $(this).find(".room-label").css("background-color", "transparent");
    $(this).find(".room-label:eq(0)").css("background-color", $(this).find(".room-label:eq(0)").css("border-color"));
  })

  $("input[type=search]").keyup(function() {
    var $str = $(this).val();
    $("#searchie").html("").width($(this).width());

    if ($str.length == 0) {
      return false;
    }

    $.each($tags, function(i, e) {
      if (e.name.indexOf($str) >= 0) {
        $("#searchie").append('<li style="padding:1rem;"><a href="'+e.link+'">'+e.name+'</a></li>');
      }
    });
  });
});