<?php
function img($file = '') {
  echo get_template_directory_uri().'/assets/images/'.$file;
  return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body>
  <?php get_template_part("partials/content", "interstitial"); ?>
  <?php get_header(); ?>
  <main>
    <?php
    if (is_home()) {
      get_template_part("partials/content", "introduction");
    } else {
      if (have_posts()) {
        while (have_posts) {
          the_post();
          get_template_part("partials/content", get_post_format());
        }
      }
    }
    ?>
  </main>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>