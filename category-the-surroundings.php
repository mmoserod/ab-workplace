<?php
function img($file = '') {
  echo get_template_directory_uri().'/assets/images/'.$file;
  return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body>
  <?php get_header(); ?>
  <main id="your-space">
    <div class="row collapse" id="home">
      <div class="column expanded" id="banner" style="background-image:url(<?php img('meetingrooms.jpg'); ?>);">
        <a class="logo">
          <img src="<?php img('logo.png'); ?>" />
        </a>
      </div>
      <div class="column medium-8 text-right expanded"><h2>THE SURROUNDINGS</h2></div>
      <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
    </div>
    <div class="row collpase" id="home-content">
      <div class="column medium-6" id="home-message">
        <p><em>In addition to the One Island East building, there is so much available in the vicinity. Here we share some information about the access, transportation and amenities around the area including the best restaurants and cafes to try.</em></p>
      </div>
      <div class="column medium-6 end">
        <img src="<?php img('thesurroundings4.jpg'); ?>">
      </div>
      <section id="the-building" class="row">
        <div class="column">
          <h3 class="sub-title text-center"><b>THE BUILDING</b></h3>
        </div>
        <div class="column medium-6">
          <img src="<?php img('amenities1.jpg'); ?>">
        </div>
        <div class="column medium-6">
          <h5 class="text-center"><b>One Island East</b></h5>
          <ul>
            <li>One Island East is a premier building in the Swire Portfolio. It is part of the Taikoo Place development which includes East Hotel, CityPlaza shopping centre and One Taikoo Place set to open in 2018.</li>
            <li>For more information on Taikoo Place, click <a href="http://www.taikooplace.com/en/Work/TaikooPlace/OneIslandEast.aspx" target="_blank">here</a></li>
          </ul>
        </div>
      </section>
      <section id="address" class="row">
        <div class="column">
          <h3 class="sub-title text-center"><b>ADDRESS</b></h3>
        </div>
        <div class="column medium-6">
          <h5 class="text-center"><b>Mailing address</b></h5>
          39F One Island East<br />
          Taikoo Place<br />
          18 Westlands Road<br />
          Quarry Bay, Hong Kong<br />
          <br />
          T: +852 2918 7888
        </div>
        <div class="column medium-6">
          <h5 class="text-center"><b>How to get to your floor</b></h5>
          <p>Take elevator from ground to 37F, take escalator up to 38F and take elevator to either 39 or 40F</p>
        </div>
      </section>
      <section id="transport" class="row">
        <div class="column">
          <h3 class="sub-title text-center"><b>TRANSPORT</b></h3>
        </div>
        <div class="column medium-6">
          <h5 class="text-center"><b>Shuttle Buses</b></h5>
          <ul>
            <li>AB will be providing free shuttle bus services between IFC and One Island East until December 31.</li>
            <li>AB to provide further detail</li>
          </ul>
          <h5 class="text-center"><b>MTR</b></h5>
          <ul>
            <li>One Island East is in the middle of Taikoo Shing & Quarry Bay Stations (6 minute walk)</li>
          </ul>
        </div>
        <div class="column medium-6">
          <h5 class="text-center"><b>Taxi</b></h5>
          <ul>
            <li>There is a taxi rank out the front of the building on Westlands Road</li>
          </ul>
          <h5 class="text-center"><b>Buses</b></h5>
          <ul>
            <li>For a list of bus lines and stops click <a href="http://www.taikooplace.com/en/Play/Transportation.aspx" target="_blank">here</a></li>
          </ul>
          <p>For more detail on transportation including travel times, click <a href="" target="_blank">here</a>.</p>
        </div>
      </section>
      <section id="amenities-nearby" class="row">
        <div class="column">
          <h3 class="sub-title text-center"><b>AMENITIES NEARBY</b></h3>
        </div>
        <div class="column text-center"><img src="<?php img('amenities2.jpg'); ?>" width="100%"></div>
      </section>
      <section id="restaurants-cafes" class="row" data-equalizer data-equalize-on="medium">
        <div class="column">
          <h3 class="sub-title text-center"><b>Restaurants &amp; Cafes</b></h3>
        </div>
        <div class="column medium-6">
          <p>For an up-to-date list of restaurants and cafes nearby, click <a href="http://www.taikooplace.com/en/Play/Dining.aspx" target="_blank">here</a></p>
        </div>
        <div id="shops-services" class="column">
          <h3 class="sub-title text-center"><b>Shops &amp; Services</b></h3>
        </div>
        <div class="column medium-6">
          <p>For an up-to-date list of shops & services nearby, click <a href="http://www.taikooplace.com/en/Play/Shops.aspx" target="_blank">here</a></p>
        </div>
        <div class="column">
          <!-- <img src="<?php img('thesurrounding2.png'); ?>"> -->
        </div>
        <div class="column medium-4" data-equalizer-watch>
          <h6 class="text-center"><b>SUPERMARKETS:</b></h6>
          <p>
            FUSION<br />
            Shop No. 2 & 3, G/F, Westlands Centre,<br />
            Westlands Road, Quarry Bay, Hong Kong
          </p>
          <p>
            APITA<br />
            LG CityPlaza<br />
            18 Tai Koo Shing Rd,<br />
            Quarry Bay, Hong Kong
          </p>
        </div>
        <div class="column medium-4" data-equalizer-watch>
          <h6 class="text-center"><b>HOTELS:</b></h6>
          <p>
            EAST HOTEL<br />
            29 Tai Koo Shing Rd,<br /> 
            Quarry Bay, Hong Kong
          </p>
          <p>
            HARBOUR PLAZA<br />
            665 King's Road<br />
            North Point, Hong Kong
          </p>
        </div>
        <div class="column medium-4" data-equalizer-watch>
          <h6 class="text-center"><b>POST OFFICE:</b></h6>
          <p>
            TAIKOO SHING POST OFFICE<br />
            G1020-1022, Kam Sing Mansion,<br />
            1-3 Tai Fung Ave, Taikoo Shing, Hong Kong
          </p>
        </div>
        <div class="column medium-4" data-equalizer-watch>
          <h6 class="text-center"><b>PHARMACY:</b></h6>
          <p>
            MANNINGS<br />
            GF Cambridge House, Taikoo Place<br />
            981 King's Rd, Quarry Bay, Hong Kong
          </p>
          <p>
            MANNINGS<br />
            Shop 420 CityPlaza 2<br />
            18 Tai Koo Shing Rd,<br />
            Quarry Bay, Hong Kong
          </p>
        </div>
        <div class="column medium-4" data-equalizer-watch>
          <h6 class="text-center"><b>FITNESS/GYMS:</b></h6>
          <p>
            PURE FITNESS<br />
            4/F & 5/F PCCW Tower Taikoo Place,<br />
            Quarry Bay, Hong Kong
          </p>
          <p>
            PHYSICAL<br />
            4, Kornhill Plaza South, 2 Kornhill Rd,<br />
            Kornhill, Hong Kong,
          </p>
        </div>
        <div class="column medium-4" data-equalizer-watch>
          <h6 class="text-center"><b>COFFEE:</b></h6>
          <p>
            PUBLIC<br />
            37F & GF One Island East<br />
            Taikoo Place<br />
            18 Westlands Road<br />
            Quarry Bay, Hong Kong
          </p>
          <p>
            STARBUCKS<br />
            Shop No. G516, Kin On Mansion<br />
            Taikoo Shing, Hong Kong<br />
          </p>
          <br /><br /><br />
        </div>
        <div class="column">
          <!-- <img src="<?php img('thesurroundings3.jpg'); ?>" width="100%"> -->
        </div>
      </section>
    </div>
  </main>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>