<header>
  <div class="row expanded collapse">
    <div class="column small-2 text-center show-for-small-only"><button id="mobilemenu"><i class="fa fa-bars"></i></button></div>
    <div class="column medium-9 hide-for-small-only expanded" style="background-color:#fff;">
      <nav>
        <ul class="dropdown menu" data-dropdown-menu>
          <li>
            <a href="<?php echo site_url(); ?>">HOME</a>
          </li><li>
            <a href="<?php echo get_category_link(2); ?>" class="down-down">YOUR SPACE</a>
            <ul class="menu">
              <li><a href="<?php echo get_category_link(2); ?>#task-chair">Task Chair</a></li>
              <li><a href="<?php echo get_category_link(2); ?>#workstation">Workstation</a></li>
              <li><a href="<?php echo get_category_link(2); ?>#trading-desk">Trading Desk</a></li>
              <li><a href="<?php echo get_category_link(2); ?>#technology">Technology</a></li>
            </ul>
          </li><li>
            <a href="<?php echo get_category_link(3); ?>" class="down-down">WORKSPACE</a>
            <ul class="menu">
              <li><a href="<?php echo get_category_link(3); ?>#workspace">Team Locations</a></li>
              <li><a href="<?php echo get_category_link(3); ?>#alternative-workpoints">Alternative Workpoints</a></li>
              <li><a href="<?php echo get_category_link(3); ?>#support-facilities">Support Facilities</a></li>
              <li><a href="<?php echo get_category_link(3); ?>#storage">Storage</a></li>
              <li><a href="<?php echo get_category_link(3); ?>#lighting">Lighting</a></li>
              <li><a href="<?php echo get_category_link(3); ?>#access">Access</a></li>
            </ul>
          </li><li>
            <a href="<?php echo get_category_link(4); ?>" class="down-down">MEETING ROOMS</a>
            <ul class="menu">
              <li><a href="<?php echo get_category_link(4); ?>#meeting-room-locations">Meeting Room Locations</a></li>
              <li><a href="<?php echo get_category_link(4); ?>#online-booking-system">Online Booking System</a></li>
              <li><a href="<?php echo get_category_link(4); ?>#technology">Technology</a></li>
              <li><a href="<?php echo get_category_link(4); ?>#furniture">Furniture</a></li>
              <li><a href="<?php echo get_category_link(4); ?>#environment-control">Environment Control</a></li>
            </ul>
          </li><li>
            <a href="<?php echo get_category_link(5); ?>">SHARED AMENITIES</a>
            <ul class="menu">
              <li><a href="<?php echo get_category_link(5); ?>#locations">Locations</a></li>
            </ul>
          </li><li>
            <a href="<?php echo get_category_link(6); ?>" class="down-down">THE SURROUNDINGS</a>
            <ul class="menu">
              <li><a href="<?php echo get_category_link(6); ?>#the-building">The Building</a></li>
              <li><a href="<?php echo get_category_link(6); ?>#address">Address</a></li>
              <li><a href="<?php echo get_category_link(6); ?>#transport">Transport</a></li>
              <li><a href="<?php echo get_category_link(6); ?>#amenities-nearby">Amenities Nearby</a></li>
              <li><a href="<?php echo get_category_link(6); ?>#restaurants-cafes">Restaurants &amp; Cafes</a></li>
              <li><a href="<?php echo get_category_link(6); ?>#shops-services">Shops &amp; Services</a></li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
    <div class="column small-10 medium-3 expanded end">
      <form id="search" action="">
        <div class="row expanded">
          <div class="column small-2 text-center">
            <button type="submit">
              <i class="fa fa-search"></i>
            </button>
          </div>
          <div class="column small-10 end">
            <input type="search" placeholder="SEARCH " />
          </div>
        </div>
      </form>
    </div>
  </div>
</header>