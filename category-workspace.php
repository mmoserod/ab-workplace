<?php
function img($file = '') {
  echo get_template_directory_uri().'/assets/images/'.$file;
  return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body>
  <?php get_header(); ?>
  <main id="your-space">
    <div class="row collapse" id="home">
      <div class="column expanded" id="banner" style="background-image:url(<?php img('workspace.jpg'); ?>);">
        <a class="logo">
          <img src="<?php img('logo.png'); ?>" />
        </a>
      </div>
      <div class="column medium-5 text-right expanded"><h2>WORKSPACE</h2></div>
      <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
    </div>
    <div class="row collpase" id="home-content">
      <div class="column medium-6" id="home-message">
        <p><em>Outside of your individual workpoint, this new office offers a variety of other areas for you and your team to work. The following section introduces you to the overall workspace and alternative spaces for you to work</em></p>
      </div>
      <div class="column medium-6 end">
        <img src="<?php img('workspace2.jpg'); ?>" alt="">
      </div>
    </div>

    <section id="workspace" class="row">
      <div class="column">
        <div class="row">
          <div class="column text-center">
            <h3><b>TEAM LOCATIONS</b></h3>
          </div>
          <div class="column">
            <p class="text-center protip">
              <i>Tip: Click on a label to view the area in the floor plan.</i>
            </p>
            <div class="buttons text-center">
              <button class="button room-label" style="background-color:#ff6b6b;border:1px solid #ff6b6b;" data-id="sellsideresearch">Sell Side Research</button>
              <button class="button room-label" style="background-color:#ffc000;border:1px solid #ffc000;" data-id="tradingsales">Trading + Sales</button>
              <button class="button room-label" style="background-color:#bf9000;border:1px solid #bf9000;" data-id="legal">Legal</button>
              <button class="button room-label" style="background-color:#6bd1f6;border:1px solid #6bd1f6;" data-id="corporateteams">Corporate Teams</button>
            </div>
          </div>
          <div class="column medium-6 medium-offset-3">
            <div class="canvas">
              <img src="<?php img('39f.png'); ?>">
              <img class="room" data-id="sellsideresearch" src="<?php img('AB-floor-hints-sellsidereasearch.png'); ?>">
              <img class="room" data-id="legal" src="<?php img('AB-floor-hints-legal.png'); ?>">
              <img class="room" data-id="tradingsales" src="<?php img('AB-floor-hints-tradingsales.png'); ?>">
              <img class="room" data-id="corporateteams" src="<?php img('AB-floor-hints-corporateteams.png'); ?>">
            </div>
            <h4 class="text-center"><strong>39F</strong></h4>
          </div>
          <div class="column">
            <p class="text-center protip" style="padding-top:2rem;">
              <i>Tip: Click on a label to view the area in the floor plan.</i>
            </p>
            <div class="buttons text-center">
              <button class="button room-label" style="background-color:#ef96e5;border:1px solid #ef96e5;" data-id="fixedincome">Fixed Income</button>
              <button class="button room-label" style="background-color:#7030a0;border:1px solid #7030a0;" data-id="trading">Trading</button>
              <button class="button room-label" style="background-color:#c3956f;border:1px solid #c3956f;" data-id="operations">Operations</button>
              <button class="button room-label" style="background-color:#6bd199;border:1px solid #6bd199;" data-id="equities">Equities</button>
              <button class="button room-label" style="background-color:#ac87c8;border:1px solid #ac87c8;" data-id="bd">BD</button>
              <button class="button room-label" style="background-color:#92d050;border:1px solid #92d050;" data-id="clientteams">Client Teams</button>
            </div>
          </div>
          <div class="column medium-6 medium-offset-3 end">
            <div class="canvas">
              <img src="<?php img('40f.png'); ?>">
              <img class="room" data-id="fixedincome" src="<?php img('AB-floor-hints-fixedincome.png'); ?>">
              <img class="room" data-id="trading" src="<?php img('AB-floor-hints-trading.png'); ?>">
              <img class="room" data-id="operations" src="<?php img('AB-floor-hints-operations.png'); ?>">
              <img class="room" data-id="legalauditcompliance" src="<?php img('AB-floor-hints-legalauditcompliance.png'); ?>">
              <img class="room" data-id="equities" src="<?php img('AB-floor-hints-equities.png'); ?>">
              <img class="room" data-id="bd" src="<?php img('AB-floor-hints-bd.png'); ?>">
              <img class="room" data-id="clientteams" src="<?php img('AB-floor-hints-clientteams.png'); ?>">
            </div>
            <h4 class="text-center"><strong>40F</strong></h4>
          </div>
        </div>
      </div>

      <div class="column" style="background-color:#efefef;margin-top:2rem;">
        <div class="row">
          <div id="alternative-workpoints" class="column text-center">
            <h3><b>ALTERNATIVE WORKPOINTS</b></h3>
            <p class="text-center protip">
              <i>Tip: Click on a label to view the area in the floor plan.</i>
            </p>
            <div class="buttons text-center">
              <button class="button room-label" style="background-color:#ffdf4e;border:1px solid #ffdf4e;" data-id="huddlerooms">Huddle Rooms</button>
              <button class="button room-label" style="background-color:#b1d34a;border:1px solid #b1d34a;" data-id="quietbooths">Quiet Booths</button>
              <button class="button room-label" style="background-color:#09939f;border:1px solid #09939f;" data-id="phonebooths">Phone Booths</button>
            </div>
          </div>
          <div class="column medium-6">
            <div class="canvas">
              <img src="<?php img('39f.png'); ?>">
              <img class="room" data-id="huddlerooms" src="<?php img('AB-floor-hints-huddlerooms.png'); ?>">
              <img class="room" data-id="quietbooths" src="<?php img('AB-floor-hints-quietbooths.png'); ?>">
              <img class="room" data-id="phonebooths" src="<?php img('AB-floor-hints-phonebooths.png'); ?>">
            </div>
            <h4 class="text-center"><strong>39F</strong></h4>
          </div>
          <div class="column medium-6">
            <div class="canvas">
              <img src="<?php img('40f.png'); ?>">
              <img class="room" data-id="huddlerooms" src="<?php img('AB-floor-hints-huddlerooms2.png'); ?>">
              <img class="room" data-id="quietbooths" src="<?php img('AB-floor-hints-quietbooths2.png'); ?>">
              <img class="room" data-id="phonebooths" src="<?php img('AB-floor-hints-phonebooths2.png'); ?>">
            </div>
            <h4 class="text-center"><strong>40F</strong></h4>
          </div>
          <div class="column">
            <div class="canvas-info text-center">
              <a class="button hollow" data-fancybox data-id="huddlerooms" href="<?php img('HuddleRoom.png'); ?>">INFORMATION</a>
              <a class="button hollow" data-fancybox data-id="quietbooths" href="<?php img('QuietRoom.png'); ?>">INFORMATION</a>
              <a class="button hollow" data-fancybox data-id="phonebooths" href="<?php img('Phonebooth.png'); ?>">INFORMATION</a>
            </div>
          </div>
        </div>
      </div>
      <div class="column">
        <div class="row">
          <div id="support-facilities" class="column text-center">
            <h3><b>SUPPORT FACILITIES</b></h3>
            <p class="text-center protip">
              <i>Tip: Click on a label to view the area in the floor plan.</i>
            </p>
            <div class="buttons text-center">
              <button class="button room-label" style="background-color:#9ac5e8;border:1px solid #9ac5e8;" data-id="lockers">Lockers</button>
              <button class="button room-label" style="background-color:#ff6a69;border:1px solid #ff6a69;" data-id="printcopyrooms">Printcopy Rooms</button>
              <button class="button room-label" style="background-color:#0891a4;border:1px solid #0891a4;" data-id="waterpoints">Water Points</button>
            </div>
          </div>
          <div class="column medium-6">
            <div class="canvas">
              <img src="<?php img('39f.png'); ?>">
              <img class="room" data-id="lockers" src="<?php img('AB-floor-hints-lockers.png'); ?>">
              <img class="room" data-id="printcopyrooms" src="<?php img('AB-floor-hints-printcopyrooms.png'); ?>">
              <img class="room" data-id="waterpoints" src="<?php img('AB-floor-hints-waterpoints.png'); ?>">
            </div>
            <h4 class="text-center"><strong>39F</strong></h4>
          </div>
          <div class="column medium-6">
            <div class="canvas">
              <img src="<?php img('40f.png'); ?>">
              <img class="room" data-id="lockers" src="<?php img('AB-floor-hints-lockers2.png'); ?>">
              <img class="room" data-id="printcopyrooms" src="<?php img('AB-floor-hints-printcopyrooms2.png'); ?>">
              <img class="room" data-id="waterpoints" src="<?php img('AB-floor-hints-waterpoints2.png'); ?>">
            </div>
            <h4 class="text-center"><strong>40F</strong></h4>
          </div>
          <div class="column">
            <div class="canvas-info text-center">
              <a class="button hollow" data-fancybox data-id="lockers" href="<?php img('Lockers.png'); ?>">INFORMATION</a>
              <a class="button hollow" data-fancybox data-id="printcopyrooms" href="<?php img('CopyRoom.png'); ?>">INFORMATION</a>
              <a data-fancybox data-id="waterpoints" src="<?php img('blank.png'); ?>"></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="row" style="background-color:#efefef;padding-top:2rem;padding-bottom:2rem;">
      <div id="storage" class="column">
        <h2 class="sub-title text-center"><b>STORAGE</b></h2><br />
      </div>
      <div class="column medium-6">
        <h5><b>Lockers</b></h5>
        <p>Request a locker from Admin Services. They will help you set up your own personal combination.</p>
        <p>Remember there won’t be enough lockers for everyone, so please be considerate of other people. There is personal storage at your desk and team storage in your workspace. There are also coat cupboards at each entry point.</p>
        <h5><b>Off-site storage</b></h5>
        <ul>
          <li>Admin Services to provide details</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>Team storage</b></h5>
        <p>Team storage has been determined based on each teams requirements and consists of hanging filing and/or shelving.</p>
        <p>To understand more about your allocation to team storage, speak to your business head.</p>
        <p>Each bay is lockable by key. If your team has lost your key, see Admin Services.</p>
      </div>
    </div>
    <div class="row" style="padding-top:2rem;padding-bottom:2rem;">
      <div id="lighting" class="column">
        <h2 class="sub-title text-center"><b>LIGHTING</b></h2><br />
      </div>
      <div class="column medium-6 end">
        <h5><b>Office lighting</b></h5>
        <ul>
          <li>General office lighting will be on between 9am-5:30pm.</li>
          <li>After hours lighting is motion sensor controlled, so no need to switch anything on or off.</li>
          <li>Enclosed meeting rooms and quiet rooms are on motion sensors with lighting control in the formal meeting rooms.</li>
        </ul>
      </div>
    </div>
    <section class="row" style="padding-bottom:2rem;padding-top:2rem;background-color:#efefef;">
      <div id="access" class="column">
        <h2 class="sub-title text-center"><b>ACCESS</b></h2>
        <p class="text-center protip">
          <i>Tip: Click on a label to view the area in the floor plan.</i>
        </p>
      </div>
      <div class="column">
        <div class="buttons text-center">
          <button class="button room-label" style="background-color:#6bd1f6;border:1px solid #6bd1f6;" data-id="openaccesstoallstaff">Open Access to All Staff</button>
          <button class="button room-label" style="background-color:#ffdf4e;border:1px solid #ffdf4e;" data-id="outofhoursswipeaccess">Out of Hours Swipe Access</button>
          <button class="button room-label" style="background-color:#ff6b6b;border:1px solid #ff6b6b;" data-id="swipeaccessdoors">Swipe Access Doors</button>
        </div>
      </div>
      <div class="column medium-6">
        <div class="canvas">
          <img src="<?php img('39f.png'); ?>">
          <img class="room" data-id="openaccesstoallstaff" src="<?php img('AB-floor-hints-openaccesstoallstaff.png'); ?>" />
          <img class="room" data-id="outofhoursswipeaccess" src="<?php img('AB-floor-hints-outofhoursswipeaccess.png'); ?>" />
          <img class="room" data-id="swipeaccessdoors" src="<?php img('AB-floor-hints-swipeaccessdoors.png'); ?>" />
        </div>
        <h4 class="text-center"><strong>39F</strong></h4>
      </div>
      <div class="column medium-6">
        <div class="canvas">
          <img src="<?php img('40f.png'); ?>">
          <img class="room" data-id="openaccesstoallstaff" src="<?php img('AB-floor-hints-openaccesstoallstaff2.png'); ?>" />
          <img class="room" data-id="outofhoursswipeaccess" src="<?php img('blank.png'); ?>" />
          <img class="room" data-id="swipeaccessdoors" src="<?php img('AB-floor-hints-swipeaccessdoors2.png'); ?>" />
        </div>
        <h4 class="text-center"><strong>40F</strong></h4>
      </div>
      <div class="column medium-6">
        <h5><b>Office hours</b></h5>
        <ul>
          <li>Main entry on 39F will be open between 9am – 5.30pm Monday to Friday</li>
          <li>All other entry points will be accessible via security access card/Octopus Card</li>
          <li>Octopus cards need to be pre-authorised for out of hours access. Please contact Admin Services to register your card</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>Out of hours</b></h5>
        <ul>
          <li>Out of hours access is via your Security pass/Octopus cards. Please contact Admin Services to register your card</li>
          <li>If you need access out of hours for guests, please also see Admin Services to arrange</li>
        </ul>
      </div>
    </section>
  </main>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>