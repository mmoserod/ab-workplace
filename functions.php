<?php
function queueScripts() {
  $v = wp_get_theme();
  $v = $v['Version'];

  if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri().'/assets/js/vendor/jquery.js', false, $v);
    wp_enqueue_script('what-input', get_template_directory_uri().'/assets/js/vendor/what-input.js', ['jquery'], $v, true);
    wp_enqueue_script('foundation', get_template_directory_uri().'/assets/js/vendor/foundation.js', ['jquery', 'what-input'], $v, true);
    wp_enqueue_script('fancybox', get_template_directory_uri().'/assets/js/vendor/fancybox.js', ['jquery'], $v, true);
    wp_enqueue_script('slick', get_template_directory_uri().'/assets/js/vendor/slick.js', ['jquery'], $v, true);
    wp_enqueue_script('app', get_template_directory_uri().'/assets/js/app.js', ['jquery','what-input', 'foundation', 'fancybox', 'slick'], $v, true);
  }
}

function queueStyles() {
  $v = wp_get_theme();
  $v = $v['Version'];

  if (!is_admin()) {
    wp_enqueue_style('font-awesome', get_template_directory_uri().'/assets/css/vendor/fontawesome.css', false, $v, 'all');
    wp_enqueue_style('foundation', get_template_directory_uri().'/assets/css/vendor/foundation.css', ['font-awesome'], $v, 'all');
    wp_enqueue_style('fancybox', get_template_directory_uri().'/assets/css/vendor/fancybox.css', false, $v, 'all');
    wp_enqueue_style('slick', get_template_directory_uri().'/assets/css/vendor/slick.css', false, $v, 'all');
    wp_enqueue_style('googlefonts', 'https://fonts.googleapis.com/css?family=Roboto:300,400,700', false, $v, 'all');
    wp_enqueue_style('app', get_template_directory_uri().'/assets/css/app.css', ['font-awesome', 'foundation', 'fancybox', 'googlefonts', 'slick'], $v, 'all');
  }
}

add_theme_support('custom-logo');
add_theme_support('post-thumbnails');
add_theme_support('post-formats', array('standard', 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
add_action('init', 'queueStyles');
add_action('init', 'queueScripts');
