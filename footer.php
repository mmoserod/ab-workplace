<footer id="footer" class="row expanded">
  <div class="column"><small>AB Welcome Site</small></div>
</footer>
<ul id="searchie" class="no-bullet" style="position:fixed;top:4.3vh;right:0;background:#fff;z-index:2;"></ul>
<?php
$tags = get_tags(["hide_empty" => false]);
$keys = [];
foreach ($tags as $tag) {
  $keys[] = [
    "name" => $tag->name,
    "link" => home_url($tag->description)
  ];
}
?>
<script>
var $tags = <?php echo json_encode($keys); ?>;
</script>