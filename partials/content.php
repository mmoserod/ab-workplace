<div class="row collapse" id="home">
  <div class="column expanded" id="banner">
    <a class="logo">
      <img src="<?php img('logo.png'); ?>" />
    </a>
  </div>
  <div class="column medium-5 text-right expanded"><h2>YOUR SPACE</h2></div>
  <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
</div>
<div class="row" id="home-content">
  <div class="column medium-6" id="home-message">
    <p><em>Welcome to your new office!</em></p>
    <p><em>We hope you enjoy the space and to help you get settled, we have made this website to help you navigate the new space and tools.</em></p>
    <p><em>Feel free to have a look around and get to know your new office and new area.</em></p>
  </div>
  <div class="column medium-6">
    <iframe width="100%" height="315" src="https://www.youtube.com/embed/qjsmJGh2V-8" frameborder="0" allowfullscreen></iframe>
  </div>
</div>