<div class="row collapse" id="home">
  <div class="column expanded" id="banner">
    <a class="logo">
      <img src="<?php img('logo.png'); ?>" />
    </a>
  </div>
  <div class="column medium-7 text-right expanded"><h2 style="font-size:2.45rem;">WELCOME MESSAGE</h2></div>
  <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
</div>
<div class="row collpase" id="home-content">
  <div class="column end" id="home-message">
    <p><em>Dear ABHK Colleagues</em></p>
    <p><em>When we announced our intention to relocate our Hong Kong office to One Island East about a year ago, we had two objectives. First to create a high-quality work environment with a modern design that facilitates interaction and teamwork. Second, to manage the economics in a way that would allow us to invest in our growth and ideas that will keep us ahead of tomorrow.</em></p>
    <p><em>A year on, we are excited to welcome you to our new office. We hope you find its innovative features beneficial to you and that you enjoy the new surroundings. The Hong Kong office will be the model for upcoming changes at AB offices worldwide, and we are the proud pioneers.</em></p>
    <p><em>Over the past few weeks, the Transition Team has provided us with tips on various equipment, technologies, set-up procedures and arrangements. To wrap things up, we have combined all the useful information into the attached package to help you familiarize yourself with our new "home".</em></p>
    <p><em>If we can help you in any way to get settled in, feel free to ask us. It’s a new experience for all of us, and some glitches may be unavoidable at first. But we'll do our best to help everyone get up to speed as smoothly as possible.</em></p>
    <P><em>Finally, please join us on Oct 24 as we host our auspicious office Welcome Ceremony!</em></P>
    <br />
    <br />
    <br />
  </div>
  <!-- <div class="column medium-6 end">
    <iframe width="100%" height="315" src="https://www.youtube.com/embed/qjsmJGh2V-8" frameborder="0" allowfullscreen></iframe>
  </div> -->
</div>