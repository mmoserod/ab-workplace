<?php
function img($file = '') {
  echo get_template_directory_uri().'/assets/images/'.$file;
  return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body>
  <?php get_header(); ?>
  <main id="your-space">
    <div class="row collapse" id="home">
      <div class="column expanded" id="banner" style="background-image:url(<?php img('yourspace.jpg'); ?>);">
        <a class="logo">
          <img src="<?php img('logo.png'); ?>" />
        </a>
      </div>
      <div class="column medium-5 text-right expanded"><h2>YOUR SPACE</h2></div>
      <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
    </div>

    <div id="task-chair" class="row">
      <div class="column">
        <h2 class="text-center">TASK CHAIR</h2>
      </div>
      <div class="column medium-6">
        <h5><b>Aeron Chair</b></h5>
        <h6>By Herman Miller</h6>
        <p><img src="<?php img('taskchair1.jpg'); ?>" alt=""></p>
        <p>For more information on the specs and performance of your task chair, check out the Herman Miller webpage <a href="http://www.hermanmiller.com/products/seating/performance-work-chairs/aeron-chairs.html" target="_blank">here</a></p>
      </div>
      <div class="column medium-6 end">
        <h5><b>Need to adjust your chair?</b></h5>
        <h6>&nbsp;</h6>
        <p><img src="<?php img('taskchair2.jpg'); ?>" width="100%"></p>
        <p>Click <a href="http://brightcove.vo.llnwd.net/e1/uds/pd/2290002715001/2290002715001_5159143139001_5159129102001.mp4?playerId=2321945808001&lineupId=&affiliateId=&pubId=2290002715001&videoId=5159129102001" target="_blank">here</a> for an instructional video</p>
      </div>
    </div>
    <div id="workstation" class="row">
      <div class="column">
        <h2 class="text-center">WORKSTATION</h2>
      </div>
      <div class="column medium-6">
        <img src="<?php img('workstation1.jpg'); ?>" alt="">
      </div>
      <div class="column medium-6 end">
        <h5><b>Workstation Features</b></h5>
        <ul>
          <li>The height of the desk is adjustable! For instructions click <a href="http://www.hermanmiller.com/content/dam/hermanmiller/documents/user_information/locale_adjustment_guide.pdf" target="_blank">here</a></li>
          <li>The screen is made out of acoustic material and is pinnable</li>
          <li>Your drawers are lockable via a dial lock. Click <a href="http://www.hermanmiller.com/content/dam/hermanmiller/documents/user_information/locale_adjustment_guide.pdf" target="_blank">here</a> to reset it.</li>
          <li>The roller door key is lockable. If you lose your key, contain Admin Services.</li>
        </ul>
      </div>
    </div>
    <div id="trading-desk" class="row">
      <div class="column">
        <h2 class="text-center">TRADING DESK</h2>
      </div>
      <div class="column medium-6">
        <h5>&nbsp;</h5>
        <img src="<?php img('tradingdesk1.jpg'); ?>" alt="">
        <p>For more information on the specs and performance of your desk, check out the SBFI webpage <a href="http://www.sbfi.com/aspect-trading-desk/" target="_blank">here</a></p>
      </div>
      <div class="column medium-6 end">
        <h5><b>Need to adjust your desk height?</b></h5>
        <img src="<?php img('tradingdesk2.jpg'); ?>" width="100%">
        <p>Click <a href="http://www.sbfi.com/" target="_blank">here</a> for an instructional video</p>
      </div>
    </div>
    <div id="technology" class="row">
      <div class="column">
        <h2 class="text-center">TECHNOLOGY</h2>
      </div>
      <div class="column medium-6">
        <h5><b>Thin Client</b></h5>
        <ul>
          <li>AB IT to provide info</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>Computer protocols</b></h5>
        <ul>
          <li>AB to provide info</li>
          <li>Do you want to include instructions about logging in, switching off the screen at the end of the day...?</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>Wireless headset</b></h5>
        <ul>
          <li>Did you know the headset will work 50 meters away from your desk?</li>
          <li>You can seamlessly switch between your desk phone to a quiet room. Click <a href="http://www.sbfi.com/" target="_blank">here</a> to find out more.</li>
        </ul>
      </div>
      <div class="column medium-6 end">
        <h5><b>Desk phone</b></h5>
        <ul>
          <li>AB IT provide info</li>
        </ul>
      </div>
    </div>
  </main>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>