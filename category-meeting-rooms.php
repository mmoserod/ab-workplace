<?php
function img($file = '') {
  echo get_template_directory_uri().'/assets/images/'.$file;
  return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body>
  <?php get_header(); ?>
  <main id="your-space">
    <div class="row collapse" id="home">
      <div class="column expanded" id="banner" style="background-image:url(<?php img('meetingrooms.jpg'); ?>);">
        <a class="logo">
          <img src="<?php img('logo.png'); ?>" />
        </a>
      </div>
      <div class="column medium-7 text-right expanded"><h2>MEETING ROOMS</h2></div>
      <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
    </div>
    <div class="row collpase" id="home-content" style="padding-bottom:4rem;">
      <div class="column medium-6" id="home-message">
        <p><em>There are meeting rooms available within the workspace and outside the secure zone (external).</em></p>
        <p><em>The external meeting rooms are bookable on the online booking system <a href="" target="_blank">here</a>. Internal meeting rooms are not bookable but are shared between teams.</em></p>
      </div>
      <div class="column medium-6 end">
        <img src="<?php img('meetingrooms2.jpg'); ?>">
      </div>
    </div>
    <div id="meeting-room-locations" class="row" style="background-color:#efefef;padding-bottom:2rem;">
      <div class="column" style="padding-top:2rem;">
        <h3 class="sub-title text-center"><b>MEETING ROOM LOCATIONS</b></h3>
        <p class="text-center protip">
          <i>Tip: Click on a label to view the area in the floor plan.</i>
        </p>
      </div>
      <div class="column">
        <div class="buttons text-center">
          <button class="button room-label" style="background-color:#ffdf4e;border:1px solid #ffdf4e;" data-id="clientmeeting">Client Meeting</button>
          <button class="button room-label" style="background-color:#00b1cd;border:1px solid #00b1cd;" data-id="internalmeetingrooms">Internal Meeting Rooms</button>
          <button class="button room-label" style="background-color:#000;border:1px solid #000;color:#888;" data-id="boardroom">Board Room</button>
        </div>
      </div>
      <div class="column medium-6">
        <div class="canvas">
          <img src="<?php img('39f.png'); ?>">
          <img class="room" data-id="clientmeeting" src="<?php img('AB-floor-hints-clientmeetings.png'); ?>">
          <img class="room" data-id="internalmeetingrooms" src="<?php img('AB-floor-hints-internalmeetingrooms.png'); ?>">
          <img class="room" data-id="boardroom" src="<?php img('AB-floor-hints-boardroom.png'); ?>">
        </div>
        <h4 class="text-center"><strong>39F</strong></h4>
      </div>
      <div class="column medium-6">
        <div class="canvas">
          <img src="<?php img('40f.png'); ?>">
          <img class="room" data-id="clientmeeting" src="<?php img('AB-floor-hints-clientmeetings2.png'); ?>">
          <img class="room" data-id="internalmeetingrooms" src="<?php img('AB-floor-hints-internalmeetingrooms2.png'); ?>">
          <img class="room" data-id="boardroom" src="<?php img('blank.png'); ?>">
        </div>
        <h4 class="text-center"><strong>40F</strong></h4>
      </div>
      <div class="column">
        <div class="canvas-info text-center">
          <a href="<?php img('ClientMeetingRoom.png'); ?>" class="button hollow" data-id="clientmeeting" data-fancybox>INFORMATION</a>
          <a href="<?php img('Boardroom.png'); ?>" class="button hollow" data-id="boardroom" data-fancybox>INFORMATION</a>
          <a href="<?php img('InternalMeetingrooms.png'); ?>" class="button hollow" data-id="internalmeetingrooms" data-fancybox>INFORMATION</a>
        </div>
      </div>
    </div>
    <div id="online-booking-system" class="row" style="padding-bottom:2rem;">
      <div class="column" style="padding-top:2rem;">
        <h3 class="sub-title text-center"><b>ONLINE BOOKING SYSTEM</b></h3>
      </div>
      <div class="column medium-6">
        <h5><b>How do I book a meeting room?</b></h5>
        <ul>
          <li>IT to provide details on online booking system</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>Protocol</b></h5>
        <ul>
          <li>Admin Services to provide details eg. if no one is in the room 30 minutes after a meeting begins...</li>
        </ul>
      </div>
    </div>
    <div class="row" id="technology" style="background-color:#efefef;padding-top:2rem;padding-bottom:2rem;">
      <div class="column">
        <h3 class="sub-title text-center"><b>TECHNOLOGY</b></h3>
      </div>
      <div class="column medium-6">
        <h5><b>VC</b></h5>
        <ul>
          <li>IT to provide details</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>Polycom/Cisco</b></h5>
        <ul>
          <li>IT to provide details</li>
        </ul>
      </div>
    </div>
    <div class="row" style="padding-top:2rem;padding-bottom:2rem;">
      <div id="furniture" class="column">
        <h3 class="sub-title text-center"><b>FURNITURE</b></h3>
      </div>
      <div class="column medium-6">
        <h5><b>I need to reconfigure the furniture</b></h5>
        <ul>
          <li>For extra capacity seating, stackable chairs are available.</li>
          <li>Meeting Room A&B furniture can be reconfigured.</li>
          <li>See Admin Services for assistance.</li>
        </ul>
      </div>
      <div class="column medium-6">
        <h5><b>I need to reconfigure the room</b></h5>
        <ul>
          <li>Meeting Room A&B are combinable. Please see Admin Services if you want to reconfigure the room</li>
        </ul>
      </div>
    </div>
    <div class="row" style="background-color:#efefef;padding-top:2rem;padding-bottom:2rem;">
      <div id="environment-control" class="column">
        <h3 class="sub-title text-center"><b>ENVIRONMENT CONTROL</b></h3>
      </div>
      <div class="column medium-6 end">
        <h5><b>External Meeting Rooms</b></h5>
        <ul>
          <li>All external meeting rooms are fitted with environmental control units. You can adjust the blinds, lighting and presentation equipment from this control unit. For assistance on how to use it, contact IT.</li>
          <li>Switch activated frosting provides extra privacy for the external meeting rooms.</li>
        </ul>
      </div>
    </div>
  </main>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>