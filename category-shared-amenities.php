<?php
function img($file = '') {
  echo get_template_directory_uri().'/assets/images/'.$file;
  return true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php wp_head(); ?>
</head>
<body>
  <?php get_header(); ?>
  <main id="your-space">
    <div class="row collapse" id="home">
      <div class="column expanded" id="banner" style="background-image:url(<?php img('sharedamenities.jpg'); ?>);">
        <a class="logo">
          <img src="<?php img('logo.png'); ?>" />
        </a>
      </div>
      <div class="column medium-7 text-right expanded"><h2>SHARED AMENITIES</h2></div>
      <div class="column medium-3 text-right expanded end"><h4>Welcome Site</h4></div>
    </div>
    <div class="row collpase" id="home-content">
      <div class="column medium-6" id="home-message">
        <p><em>The new space offers numerous amenities for people to use. There is a café on 39F, a generous lounge on 40F as well as a fully equipped video recording room and a wellness room.</em></p>
        <p><em>These spaces are designed to be shared and can be utilised any time of the day.</em></p>
      </div>
      <div class="column medium-6 end">
        <img src="<?php img('sharedamenities2.jpg'); ?>">
      </div>
    </div>

    <section id="locations" class="row" style="padding-top:4rem;">
      <div class="column">
        <h3 class="sub-title text-center"><b>LOCATIONS</b></h3>
        <p class="text-center" style="color:#aaa;">
          <em>Tip: Click on a label to view the area on the floor plan.</em>
        </p>
      </div>
      <div class="column">
        <div class="buttons text-center">
          <button class="button room-label" style="background-color:#8cd545;border:1px solid #8cd545;" data-id="wellnessroom">Wellness Room</button>
          <button class="button room-label" style="background-color:#c2956e;border:1px solid #c2956e;" data-id="receptionwaiting">Reception+ Waiting</button>
          <button class="button room-label" style="background-color:#ffc000;border:1px solid #8fabdd;" data-id="stairs">Stair</button>
          <button class="button room-label" style="background-color:#ff6b6b;border:1px solid #ff6b6b;" data-id="cafe">Cafe</button>
          <button class="button room-label" style="background-color:#f9442b;border:1px solid #f9442b;" data-id="reheatpantry">Reheat Pantry</button>
          <button class="button room-label" style="background-color:#fcc103;border:1px solid #fcc103;" data-id="toilet">Toilet</button>
          <button class="button room-label" style="background-color:#0a929e;border:1px solid #0a929e;" data-id="shower">Shower</button>
          <button class="button room-label" style="background-color:#0070c0;border:1px solid #0070c0;" data-id="lounge">Lounge</button>
          <button class="button room-label" style="background-color:#00b1d4;border:1px solid #00b1d4;" data-id="videostudio">Video Studio</button>
        </div>
      </div>
      <div class="column medium-6">
        <div class="canvas">
          <img src="<?php img('39f.png'); ?>">
          <img class="room" data-id="wellnessroom" src="<?php img('blank.png'); ?>">
          <img class="room" data-id="receptionwaiting" src="<?php img('AB-floor-hints-receptionwaiting.png'); ?>">
          <img class="room" data-id="stairs" src="<?php img('AB-floor-hints-stairs.png'); ?>">
          <img class="room" data-id="cafe" src="<?php img('AB-floor-hints-cafe.png'); ?>">
          <img class="room" data-id="reheatpantry" src="<?php img('AB-floor-hints-reheatpantry.png'); ?>">
          <img class="room" data-id="toilet" src="<?php img('AB-floor-hints-toilet.png'); ?>">
          <img class="room" data-id="shower" src="<?php img('AB-floor-hints-shower.png'); ?>">
          <img class="room" data-id="lounge" src="<?php img('blank.png'); ?>">
          <img class="room" data-id="videostudio" src="<?php img('blank.png'); ?>">
        </div>
        <h4 class="text-center"><strong>39F</strong></h4>
      </div>
      <div class="column medium-6">
        <div class="canvas">
          <img src="<?php img('40f.png'); ?>">
          <img class="room" data-id="wellnessroom" src="<?php img('AB-floor-hints-wellnessroom.png'); ?>">
          <img class="room" data-id="receptionwaiting" src="<?php img('blank.png'); ?>">
          <img class="room" data-id="stairs" src="<?php img('AB-floor-hints-stairs2.png'); ?>">
          <img class="room" data-id="lounge" src="<?php img('AB-floor-hints-lounge.png'); ?>">
          <img class="room" data-id="toilet" src="<?php img('AB-floor-hints-toilet2.png'); ?>">
          <img class="room" data-id="reheatpantry" src="<?php img('AB-floor-hints-reheatpantry2.png'); ?>">
          <img class="room" data-id="cafe" src="<?php img('blank.png'); ?>">
          <img class="room" data-id="shower" src="<?php img('AB-floor-hints-shower2.png'); ?>">
          <img class="room" data-id="videostudio" src="<?php img('AB-floor-hints-videostudio.png'); ?>">
        </div>
        <h4 class="text-center"><strong>40F</strong></h4>
      </div>
      <div class="column">
        <div class="canvas-info text-center" style="height:auto !important;">
          <a class="button hollow" data-fancybox data-id="wellnessroom" href="<?php img('Wellness.png'); ?>">INFORMATION</a>
          <a class="button hollow" data-fancybox data-id="videostudio" href="<?php img('Videoroom.png'); ?>">INFORMATION</a>
          <a class="button hollow" data-fancybox data-id="shower" href="<?php img('Shower.png'); ?>">INFORMATION</a>
          <a class="button hollow" data-fancybox data-id="receptionwaiting" href="<?php img('Reception.png'); ?>">INFORMATION</a>
          <a class="button hollow" data-fancybox data-id="stairs" href="<?php img('Stair.png'); ?>">INFORMATION</a>
          <a data-id="lounge"></a>
          <a class="button hollow" data-fancybox data-id="toilet" href="<?php img('Toilets.png'); ?>">INFORMATION</a>
          <a data-id="reheatpantry"></a>
          <a data-id="cafe" data-fancybox class="button hollow" href="<?php img('Cafe.png'); ?>">INFORMATION</a>
        </div>
      </div>
    </section>
  </main>
  <?php get_footer(); ?>
  <?php wp_footer(); ?>
</body>
</html>